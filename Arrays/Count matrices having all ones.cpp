#include <bits/stdc++.h>
using namespace std;

//Count number of subarrays containing all values as K
int countIn1DArray(vector<int>& arr, int K) {
    //dp[i] = number of subarrays ending at ith position with all 1s

    int n = arr.size();
    vector<int> dp(n, 0);
    dp[0] = arr[0] == K;
    for (int i = 1; i < n; ++i) {
        if (arr[i] == K) {
            dp[i] = 1 + (arr[i - 1] == K ? dp[i - 1] : 0);
        }
    }

    int count = 0;
    for (int i = 0; i < n; ++i)
        count += dp[i];
    return count;
}

//Count number of submatrices having all values as one
int countIn2DArray(vector<vector<int>>& mat) {
    if (mat.empty() || mat[0].empty())
        return 0;
    int R = mat.size();
    int C = mat[0].size();

    int count = 0;
    for (int leftCol = 0; leftCol < C; ++leftCol) {
        vector<int> rowSum(R, 0);
        for (int rightCol = leftCol; rightCol < C; ++rightCol) {
            for (int i = 0; i < R; ++i) {
                if (mat[i][rightCol] == 1) {
                    rowSum[i]++;
                }
            }
            count += countIn1DArray(rowSum, rightCol - leftCol + 1);
        }
    }
    return count;
}