#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.spoj.com/problems/ABCDEF/
 * https://www.interviewbit.com/problems/number-of-sextuplets-or-six-values-that-satisfy-an-equation/
 * Given an array of integers A, find the number of sextuplets that satisfy the equation (a * b + c) / d - e = f. 
 * (a * b + c) = d * (e + f)
 * Find all possible values of LHS and RHS.
 * Iterate over LHS values and check if it is present in RHS value also
 */
int solve(vector<int> &A) {
    int n = A.size();
    const int MOD = 1e9 + 7;
    vector<int> LHS;
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            for (int k = 0; k < n; ++k) {
                int a = A[i], b = A[j], c = A[k];
                LHS.push_back(a * b + c);
            }

    //
    vector<int> RHS;
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            for (int k = 0; k < n; ++k) {
                int d = A[i], e = A[j], f = A[k];
                if (d != 0)
                    RHS.push_back(d * e + d * f);
            }
    //
    unordered_map<int, long long> LHS_ct, RHS_ct;
    for (int i = 0; i < LHS.size(); ++i) {
        LHS_ct[LHS[i]]++;
    }
    for (int i = 0; i < RHS.size(); ++i) {
        RHS_ct[RHS[i]]++;
    }
    long long res = 0;
    for (auto it : LHS_ct) {
        if (RHS_ct.find(it.first) == RHS_ct.end()) continue;
        res = (res + (RHS_ct[it.first] * it.second) % MOD) % MOD;
    }
    return res;
}

//O(N^3logN) Time | O(N^3) Space
int solve(vector<int> &A) {
    int n = A.size();
    vector<int> RHS;
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            for (int k = 0; k < n; ++k) {
                int d = A[i], e = A[j], f = A[k];
                if (d != 0)
                    RHS.push_back(d * e + d * f);
            }
    //
    sort(RHS.begin(), RHS.end());
    int result = 0;
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            for (int k = 0; k < n; ++k) {
                int a = A[i], b = A[j], c = A[k];
                int val = a * b + c;
                int upperInd = upper_bound(RHS.begin(), RHS.end(), val) - RHS.begin();
                int lowerInd = lower_bound(RHS.begin(), RHS.end(), val) - RHS.begin();
                result += (upperInd - lowerInd);
            }
    return result;
}

/**
 * Variation
 * https://www.spoj.com/problems/SUMFOUR/
 * count quadruplets such that a+b+c+d = 0
 * O(N^2logN) Time
 * We can use hash maps (sum->count) to get O(N^2) Time
 */
int countQuadruplets(vector<int> &A, vector<int> &B, vector<int> &C, vector<int> &D, int n) {
    vector<int> aPlusb;
    for (int a : A) {
        for (int b : B) {
            aPlusb.push_back(a + b);
        }
    }
    sort(aPlusb.begin(), aPlusb.end());

    int count = 0;
    for (int c : C) {
        for (int d : D) {
            int val = -1 * (c + d);
            //Gives TLE for using them separately
            // int upperInd = upper_bound(aPlusb.begin(), aPlusb.end(), val) - aPlusb.begin();
            // int lowerInd = lower_bound(aPlusb.begin(), aPlusb.end(), val) - aPlusb.begin();
            pair<vector<int>::iterator, vector<int>::iterator> subRangeIterators = equal_range(aPlusb.begin(), aPlusb.end(), val);
            int upperInd = subRangeIterators.second - aPlusb.begin();
            int lowerInd = subRangeIterators.first - aPlusb.begin();
            count += (upperInd - lowerInd);
        }
    }
    return count;
}
