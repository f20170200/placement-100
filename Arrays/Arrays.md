## ARRAYS
Tips = 
- If numbers in array are between 0 to n or 1 to n then we can use indexes to do some stuff.
- Remeber about duplicate number question = using slow and fast pointer algo to find starting point of cycle which is the duplicate element
- Difference Array Concept = For Range Query Updates
[Adding +1 to A[L] & -1 to A[R + 1]]
- max on right, min on left arrays usage
used in problems like max(arr[i] - arr[j]) s.t i < j or max(j - i) s.t. arr[j] > arr[i]
https://leetcode.com/problems/best-time-to-buy-and-sell-stock/solution/
https://www.geeksforgeeks.org/given-an-array-arr-find-the-maximum-j-i-such-that-arrj-arri/

- For median there should n/2 elements less than it.

## MATRIX
- Min operations in array then try to do that with median
- (i, j) => i * Cols + j (1D index) [Not i * Rows + j]

## SORTING
- If standard NlogN approach isn't good then try to think of O(N) approach using something like Bucket Sort. Maybe using indexes. Think
- Counting some inequality in arrays then try to use modified **MERGE METHOD** while doing merge sort especially for i < j type
- Sometimes you can **MERGE METHOD** only for combining arrays (Given two sorted arrays)
(Sorting squared array. Leetcode 997)
- https://www.geeksforgeeks.org/when-to-use-each-sorting-algorithms/?ref=leftbar-rightbar





# Functions 
- *max_element(heights.begin(), heights.end()) = max element



# TODO
https://practice.geeksforgeeks.org/problems/find-the-number-of-sub-arrays-having-even-sum/0 (very good)
https://www.geeksforgeeks.org/find-duplicates-in-on-time-and-constant-extra-space/
https://www.geeksforgeeks.org/find-zeroes-to-be-flipped-so-that-number-of-consecutive-1s-is-maximized/
https://www.geeksforgeeks.org/find-the-smallest-positive-number-missing-from-an-unsorted-array/ (index based)