#include <bits/stdc++.h>
using namespace std;
#define ppi pair<int, int>

//TODO: https://leetcode.com/problems/minimum-number-of-arrows-to-burst-balloons/discuss/93735/a-concise-template-for-overlapping-interval-problem

/**
 * Two sets of problems
 * 1) Where we sort by start time or end time and checking overlapping
 * 2) Sorting by start time or end time and checking the overlapping just won't work. You need to check for every pair.
 *    So combine start & end times and according to them find out the answer.
 */

// <---------------------- TYPE 1 ------------------------>

//Sort By End Time

//Activity selection problem = maximize non-overlapping intervals
//Variation = https://www.interviewbit.com/problems/chain-of-pairs/
//Variation = https://www.geeksforgeeks.org/find-maximum-meetings-in-one-room/ (Only one room present and we want to schedule as many meetings as possible)
struct Activitiy {
    int start, finish;
};
bool activityCompare(Activitiy s1, Activitiy s2) {
    return (s1.finish < s2.finish);
}
void printMaxActivities(Activitiy arr[], int n) {
    // Sort jobs according to finish time
    sort(arr, arr + n, activityCompare);

    // The first activity always gets selected
    int i = 0;
    //print interval arr[i]

    // Consider rest of the activities
    for (int j = 1; j < n; j++) {
        // If this activity has start time greater than or
        // equal to the finish time of previously selected
        // activity, then select it
        if (arr[i].finish <= arr[j].start) {
            //print interval arr[j]
            i = j;
        }
    }
}

//Weighted job scheduling another sort by end time (IMP)

//Sort by start time

//https://leetcode.com/problems/merge-intervals/
class MergeOverlappingIntervals {
   public:
    vector<vector<int>> merge(vector<vector<int>>& intervals) {
        if (intervals.empty() || intervals.size() == 1) return intervals;

        vector<vector<int>> res;
        //Sort by start times
        sort(intervals.begin(), intervals.end());
        auto prev = intervals[0];
        for (int i = 1; i < intervals.size(); ++i) {
            auto curr = intervals[i];
            if (prev[1] >= curr[0]) {
                //prev & curr are overlapping. so merge them
                prev[1] = max(prev[1], curr[1]);
            } else {
                res.push_back(prev);
                prev = curr;
            }
        }
        res.push_back(prev);
        return res;
    }
};

//Variation = https://www.interviewbit.com/problems/merge-intervals/
//Insert new interval into list of intervals and merge them.
//Since initially the given array is sorted, insert the new interval in the appropriate position to avoid unnecessary sorting again
class InsertIntervalAndMerge {
   public:
    vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int> newInterval) {
        int length = intervals.size();
        if (length == 0)
            return {newInterval};
        //Insert newInterval at right position O(N) time
        //We want to maintain the intervals as sorted wrt start time only

        if (newInterval[0] <= intervals[0][0]) {
            intervals.insert(intervals.begin(), newInterval);
        } else if (intervals[length - 1][0] <= newInterval[0]) {
            intervals.push_back(newInterval);
        } else {
            //Interval must be inserted somewhere inside
            for (int i = 0; i < length - 1; ++i) {
                vector<int> curr = intervals[i];
                vector<int> next = intervals[i + 1];
                if (curr[0] <= newInterval[0] && newInterval[0] <= next[0]) {
                    intervals.insert(intervals.begin() + i + 1, newInterval);
                    break;
                }
            }
        }
        // Merge Overlapping Intervals
        MergeOverlappingIntervals obj;
        return obj.merge(intervals);
    }
};

//https://leetcode.com/problems/interval-list-intersections
class IntersectingIntervals {
   public:
    vector<vector<int>> intervalIntersection(vector<vector<int>>& A, vector<vector<int>>& B) {
        vector<vector<int>> intervals;

        int i = 0, j = 0;
        while (i < A.size() && j < B.size()) {
            int s1 = A[i][0], e1 = A[i][1];
            int s2 = B[j][0], e2 = B[j][1];

            //Intersect condition
            //The intersection portion will be max(s1, s2) --- min(e1, e2)
            //So if s <= e then only that portion exists
            int s = max(s1, s2), e = min(e1, e2);
            if (s <= e) {
                intervals.push_back({s, e});
            }

            if (e1 <= e2) {
                ++i;
            } else {
                ++j;
            }
        }
        return intervals;
    }
};

//https://www.geeksforgeeks.org/job-scheduling-two-jobs-allowed-time/

// <---------------------- TYPE 2 ------------------------>

/**
 * https://www.geeksforgeeks.org/minimum-number-platforms-required-railwaybus-station/ (start & end should be different. so arrive comes before depart if they are same value)
 * IMP.
 * Why doesn't finding maximum overlapping intervals work?
 * [1,2] [3,4] [5,6] [7,8] [3.5,10]
 * Overlapping intervals = 4 but min platforms required is 2 only
 * Variation = https://www.geeksforgeeks.org/find-the-point-where-maximum-intervals-overlap/ (Max Guest in a party)
 */
class MinPlatformsRequired {
   public:
    int minPlatforms(vector<int> arrival, vector<int> depart, int n) {
        vector<ppi> times;
        int arrival_code = 0;
        for (int i = 0; i < n; ++i) {
            times.push_back({arrival[i], 0});
            times.push_back({depart[i], 1});
        }
        sort(times.begin(), times.end());
        int curr = 0;
        int res = 0;
        for (ppi t : times) {
            if (t.second == arrival_code)
                curr++;
            else
                curr--;
            res = max(res, curr);
        }
        return res;
    }
};

//https://www.interviewbit.com/problems/hotel-bookings-possible/ (start & end time of 2 intervals can be same. so depart comes first before arrive if they are same value)
class HotelBookingsPossible {
   public:
    bool hotel(vector<int>& arrive, vector<int>& depart, int K) {
        int rooms_needed = 0;
        vector<ppi> arr;
        for (int i = 0; i < arrive.size(); ++i) {
            arr.push_back({arrive[i], 1});
            arr.push_back({depart[i], 0});
        }
        sort(arr.begin(), arr.end());
        for (ppi day : arr) {
            if (day.second == 1)
                rooms_needed++;
            else if (day.second == 0)
                rooms_needed--;
            if (rooms_needed > K) return false;
        }
        if (rooms_needed > K) return false;
        return true;
    }
};