#include <bits/stdc++.h>
using namespace std;

//https://leetcode.com/problems/squares-of-a-sorted-array

vector<int> sortedSquares(vector<int>& A) {
    //O(NlogN) Time | O(1) Space
    for (int& num : A)
        num *= num;
    sort(A.begin(), A.end());
    return A;

    //O(N) Time | O(N) Space for storing merge result
    //Using Merge method
    int negativeStart = -1;
    int N = (int)A.size();
    for (int i = 0; i < N; ++i) {
        if (A[i] < 0)
            negativeStart = i;
        A[i] *= A[i];
    }

    int positiveStart = negativeStart + 1;

    //[0....negativeStart] is sorted in decreasing
    //[positiveStart.....N-1] is sorted in increasing
    vector<int> mergedResult(N);
    for (int ind = 0; ind < N; ++ind) {
        int num;
        if (positiveStart >= N) {
            //Only negative portion left
            num = A[negativeStart--];
        } else if (negativeStart < 0 || A[positiveStart] < A[negativeStart]) {
            //Only positive portion left or positive element less than negative
            num = A[positiveStart++];
        } else {
            num = A[negativeStart--];
        }
        mergedResult[ind] = num;
    }

    return mergedResult;
}