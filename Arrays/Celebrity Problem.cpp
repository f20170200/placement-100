#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/the-celebrity-problem/
 * 1. Construct graph and find vertex with indegree N-1 and outdegree 0
 * 2. Use stack. Push all elements into stack. Pop 2 elements A, B. If A knows B then A isn't celebrity so push B back else vice-versa
 *    Finally check if the top guy is really celebrity. 3(N-1) comparisions
 * 3. Two Pointers. i = 0, j = N - 1 (N - 1)
 *    If arr[i] knows arr[j] then ++i
 *    else --j
 *    Finally check the one remaining left if is a celebrity (N - 1)
 *   Total 2(N - 1) comparisions
 * 4. Array method.Given below. Same 2(N - 1) comparisions
 * 
 */
//0...N-1 people. Find celebrity
int celebrityProblem(int n) {
    int candidate = 0;  //0th guy is candidate
    for (int i = 1; i < n; ++i) {
        if (knows(candidate, i)) candidate = i;  //N-1 Comparisions
    }
    for (int i = 0; i < n; ++i) {
        if (i != candidate && knows(candidate, i) || !knows(i, candidate)) return -1;  //N - 1 comparisions
    }
    return candidate;
}

bool knows(int i, int j) {}