#include <bits/stdc++.h>
using namespace std;

//https://www.interviewbit.com/problems/largest-number/

bool comp(int a, int b) {
    //return true if a should come before b
    string s1 = to_string(a), s2 = to_string(b);
    return (s1 + s2 >= s2 + s1);
}

string largestNumber(const vector<int> &A) {
    vector<int> arr = A;
    sort(arr.begin(), arr.end(), comp);
    string result = "";
    for (int num : arr) {
        result += to_string(num);
    }
    //If first char itself is 0 then number must be zero
    if (result[0] == '0') return "0";
    return result;
}
