/**
 * https://www.geeksforgeeks.org/minimize-the-maximum-difference-between-adjacent-elements-in-an-array/
 * main problem -> finding the subarray of size N-K whose max diff b/w adjcanet eles is min.
 *              -> finding max. of every window in difference array of size N-K-1 -> using deque for finding max in every window (Sliding Window.cpp)
 */