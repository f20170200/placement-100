/**
 * https://www.geeksforgeeks.org/maximum-xor-value-of-maximum-and-second-maximum-element-among-all-possible-subarrays/
 * O(N) Time | O(N) Space Solution
 * Since need smallest subarray = the 2nd largest & largest must be at corners of subarrays
 * Compute NGR and NGL for each element
 * For each element arr[i] assume it is 2nd largest element in a subarray towards right. It's NGR[i] is the largest in the subarray
 * If you take the largest on right that doesn't work since arr[i] may not be 2nd largest in that case
 * Similarily it's NGL[i] is the largest in the subarray towards left.
 * Do this for each element and get the maximum.
 * You can use method for XOR, AND, OR (doesn't depend on it)
 */