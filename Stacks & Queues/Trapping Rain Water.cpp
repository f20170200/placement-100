#include <bits/stdc++.h>
using namespace std;

/**
 * Many methods to solve
 * 1. Using two arrays.
 * 2. Two Pointers Approach (O(1) Space)
 */

//Using two arrays
int trap1(vector<int> &heights) {
    int n = heights.size();
    int maxOnRight[n], maxOnLeft[n];
    int width = 1;
    //Pre-compute max height on right side
    maxOnRight[n - 1] = 0;
    for (int i = n - 2; i >= 0; --i) {
        maxOnRight[i] = max(maxOnRight[i + 1], heights[i + 1]);
    }
    //Pre-compute max height on left side
    maxOnLeft[0] = 0;
    for (int i = 1; i < n; ++i) {
        maxOnLeft[i] = max(maxOnLeft[i - 1], heights[i - 1]);
    }
    int totalArea = 0;
    for (int i = 0; i < n; ++i) {
        int minHeight = min(maxOnRight[i], maxOnLeft[i]);  //height by which this buidling is bounded
        if (minHeight - heights[i] > 0)
            totalArea += (minHeight - heights[i]) * width;
    }
    return totalArea;
}

//Constant space. Two pointers
int trap2(vector<int> &heights) {
    int n = heights.size();
    int totalArea = 0;
    int leftMax = heights[0];
    int rightMax = heights[n - 1];
    int left = 1, right = n - 2;
    while (left <= right) {
        /**
         * leftMax.....left.....right.....rightMax
         * if leftMax <= rightMax then we found min height bound for arr[left]
         * But we didn't still find for right since in between [left.....right] we can still get a leftMax for arr[right].
         * So we compute area for arr[left]. If (leftMax - arr[left]) was negative then arr[left] is the new leftMax
         * Similarily for other case
         */
        if (leftMax <= rightMax) {
            int diff = leftMax - heights[left];
            totalArea += (diff > 0) ? diff : 0;
            if (diff < 0) {
                leftMax = heights[left];
            }
            left++;
        } else {
            int diff = rightMax - heights[right];
            totalArea += (diff > 0) ? diff : 0;
            if (diff < 0) {
                rightMax = heights[right];
            }
            right--;
        }
    }
    return totalArea;
}