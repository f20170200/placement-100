#include "Tree.h"

/**
 * 1. Construct binary tree using inorder + preorder. Then do postorder traversal on that tree and compare it with the given postorder.
 * 2. Without extra space. By just recursive comparisions. You can use 3 pairs of indexes if you don't want to send subarrays
 */

bool isSameTree(int inorder[], int preorder[], int postorder[], int N) {
    //N represents size of the current tree
    if (N == 0)
        return true;

    if (N == 1)
        return inorder[0] == preorder[0] && preorder[0] == postorder[0];

    int root_data = preorder[0];
    int index = -1;
    for (index = 0; index < N; ++index)
        if (inorder[index] == root_data)
            break;

    //If we don't find root in inorder then they are not same
    if (index == -1)
        return true;

    /*
        Recursively check for left & right subtrees.
    */
    bool isSameLeft = isSameTree(inorder, preorder + 1, postorder, index);
    bool isSameRight = isSameTree(inorder + index + 1, preorder + index + 1, postorder + index, N - 1 - index);

    return isSameLeft && isSameRight;
}