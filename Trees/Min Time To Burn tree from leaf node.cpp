#include "Tree.h"

//Additional information stored at each node
struct Info {
    int depth;     //depth of the tree at this node
    bool isFound;  //if we found the leaf node in this tree
    int dist;      //if leaf is found then what is the dist from that node to that leaf
    Info(int depth, bool isFound, int dist) : depth(depth), isFound(isFound), dist(dist) {}
};

Info minTime(Node *root, int leaf, int &result) {
    if (!root) {
        return Info(0, false, -1);
    }
    if (root->data == leaf) {
        return Info(1, true, 0);
    }

    Info left = minTime(root->left, leaf, result);
    Info right = minTime(root->right, leaf, result);

    int dist = -1;  //dist => distance from this node to leaf
    if (left.isFound) {
        dist = left.dist + 1;
        result = max(result, dist + right.depth);
    } else {
        dist = right.dist + 1;
        result = max(result, dist + left.depth);
    }
    return Info(max(left.depth, right.depth) + 1, left.isFound || right.isFound, dist);
}

int solve(Node *root, int leaf) {
    int result = 0;
    minTime(root, leaf, result);
    return result;
}