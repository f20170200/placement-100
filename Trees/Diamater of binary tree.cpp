#include "Tree.h"

int diameter(Node* root, int& res) {
    if (!root) return 0;
    int l = diameter(root->left, res);
    int r = diameter(root->right, res);

    res = max(res, l + r + 1);
    return 1 + max(l, r);
}