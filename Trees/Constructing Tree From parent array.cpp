#include "Tree.h"

Node* createTree(int parent[], int n) {
    unordered_map<int, Node*> ptr;
    for (int i = 0; i < n; ++i) {
        Node* temp = new Node(i);
        ptr[i] = temp;
    }
    Node* root = NULL;
    for (int i = 0; i < n; ++i) {
        if (parent[i] == -1) {
            root = ptr[i];
            continue;
        }
        Node* temp = ptr[parent[i]];
        if (temp->left == NULL) {
            temp->left = ptr[i];
        } else {
            temp->right = ptr[i];
        }
    }
    return root;
}