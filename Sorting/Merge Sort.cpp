#include <bits/stdc++.h>
using namespace std;

void merge(vector<int> &arr, int start, int mid, int end) {
    int size = end - start + 1;
    vector<int> mergedResult(size);
    int ptr1 = start, ptr2 = mid + 1;  //Points to starting of the 1st half and 2nd half respectively
    for (int i = 0; i < size; ++i) {
        int num;
        if (ptr1 > mid) {
            //Only 2nd half left
            num = arr[ptr2++];
        } else if (ptr2 > end || arr[ptr1] < arr[ptr2]) {
            //Only 1st half is left or 1st half curr value < 2nd hafl curr value
            num = arr[ptr1++];
        } else {
            //2nd half curr value < 1st half curr value
            num = arr[ptr2++];
        }
        mergedResult[i] = num;
    }
    //Merged result contains sorted array for arr[start....end]. Persist the result in original array
    for (int num : mergedResult) {
        arr[start++] = num;
    }
}

void mergeSortRecursive(vector<int> &arr, int start, int end) {
    if (start < end) {
        int mid = start + (end - start) / 2;
        mergeSortRecursive(arr, start, mid);
        mergeSortRecursive(arr, mid + 1, end);
        /**
         * [start....mid] and [mid+1....end] are sorted so merge them
         * IMP = Count inequalities if you want now since sorted arrays.
         */
        merge(arr, start, mid, end);
    }
}

void mergeSort(vector<int> arr) {
    mergeSortRecursive(arr, 0, arr.size() - 1);
    for (int num : arr) cout << num << " ";
    cout << endl;
}

int main() {
    mergeSort({2, 3, 1, 5, 7, 10, 6, 15, 0});
}
