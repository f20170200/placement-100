#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/square-root-of-an-integer/
 * Floor of square root of integer
 * Using linear search. O(sqrt(N))
 * Using binary search. O(logN)
 */
int floorSquareRoot(int n) {
    if (n == 0)
        return 0;

    int low = 1, high = n;
    while (low < high) {
        int mid = low + (high - low + 1) / 2;
        if (mid * mid <= n) {
            low = mid;
        } else {
            high = mid - 1;
        }
    }
    return low;
}

/**
 * https://www.geeksforgeeks.org/find-square-root-number-upto-given-precision-using-binary-search/
 * Square root of integer upto a given precision
 * 1. Find integral part of square root O(logN) using BS
 * 2. Find the decimal portion O(P) since real values can't use BS to get the exact value
 */
double squareRoot(int N, int P) {
    int integerPart = floorSquareRoot(N);

    //Perfect square
    if (integerPart * integerPart == N)
        return double(integerPart);

    //Finding decimal portion
    double increment = 0.1;
    double answer = double(integerPart);
    for (int precision = 1; precision <= P; ++precision) {
        long double limit = 10 * increment;
        while (answer * answer <= N && increment < limit) {
            answer += increment;
        }
        //At this point answer^2 > N so subtract the increment
        answer -= increment;
        increment /= 10;
    }
    return answer;
}

//Unit tests
int main() {
    cout << floorSquareRoot(16) << endl;
    cout << floorSquareRoot(10) << endl;
    cout << floorSquareRoot(20) << endl;

    cout << squareRoot(16, 1) << endl;
    cout << squareRoot(10, 5) << endl;
    cout << squareRoot(20, 3) << endl;
    cout << squareRoot(50, 3) << endl;
    cout << squareRoot(10, 4) << endl;
}