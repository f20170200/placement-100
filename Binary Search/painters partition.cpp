#include <bits/stdc++.h>
using namespace std;

/**
 *  https://www.interviewbit.com/problems/painters-partition-problem/
 * As it is same as book allocation
 * Check DP solution also (https://www.geeksforgeeks.org/painters-partition-problem/)
*/
#define ll long long
const ll MOD = 1e9 + 7;

bool isPossible(vector<int> &board, int K, ll maxSum) {
    ll currSum = 0;
    int totalPainters = 1;
    for (int i = 0; i < board.size(); ++i) {
        if (currSum + board[i] <= maxSum) {
            currSum += board[i];
        } else {
            ++totalPainters;
            if (totalPainters > K || board[i] > maxSum)
                return false;
            currSum = board[i];
        }
    }
    return true;
}

int paint(int K, int B, vector<int> &board) {
    ll sum = 0;
    for (int num : board)
        sum += num;

    ll lo = 0, hi = sum;
    ll ans = INT_MAX;
    while (lo <= hi) {
        ll mid = lo + (hi - lo) / 2;
        if (isPossible(board, K, mid)) {
            ans = min(ans, mid);
            hi = mid - 1;
        } else {
            lo = mid + 1;
        }
    }
    return (ans * B) % MOD;
}
