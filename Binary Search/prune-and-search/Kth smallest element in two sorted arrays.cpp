#include <bits/stdc++.h>
using namespace std;

/**
 * Find kth smallest element in the combined array of the two sorted arrays
 * Kth smallest element is same as kth element in a sorted array
 * Use Prune-and-search
 * O(log(M + N)) Time | O(1) Space
 */

int kth_smallest_two_sorted_arrays(vector<int> A, vector<int> B, int A_left, int A_right, int B_left, int B_right, int K) {
    if (A_left > A_right)
        return B[B_left + K - 1];
    if (B_left > B_right)
        return A[A_left + K - 1];

    int A_mid = (A_left + A_right) / 2;
    int B_mid = (B_left + B_right) / 2;

    int X = A_mid - A_left, Y = B_mid - B_left;

    if (A[A_mid] <= B[B_mid]) {
        if (K <= (X + Y + 1)) {
            return kth_smallest_two_sorted_arrays(A, B, A_left, A_right, B_left, B_mid - 1, K);
        } else {
            return kth_smallest_two_sorted_arrays(A, B, A_mid + 1, A_right, B_left, B_right, K - (X + 1));
        }
    } else {
        return kth_smallest_two_sorted_arrays(B, A, B_left, B_right, A_left, A_right, K);
    }
}

//Unit tests
int main() {
    cout << kth_smallest_two_sorted_arrays({0, 1, 2}, {3, 4, 5, 6, 7}, 0, 2, 0, 4, 5);
}