#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/find-the-row-with-maximum-number-1s/
 * Return index of row with maximum ones
 * 1. For every row, count number of 1s. O(M*N)
 * 2. For every row, do lower_bound for 1. O(M*logN)
 * 3. Find left most one index in 1st row and then if there are less rows in 2nd row then skip it and move on to next rows
 *     O(M + N) Time
 */
int row_with_max_ones(vector<vector<int>> &matrix, int m, int n) {
    int max_ones_row_ind = 0;
    int leftmost_one_ind = lower_bound(matrix[0].begin(), matrix[0].end(), 1) - matrix[0].begin();
    if (leftmost_one_ind == n)
        leftmost_one_ind = n - 1;
    leftmost_one_ind--;
    for (int row = 1; row < m; ++row) {
        if (matrix[row][leftmost_one_ind] == 1) {
            while (leftmost_one_ind >= 0 && matrix[row][leftmost_one_ind] == 1) {
                leftmost_one_ind--;
            }
            max_ones_row_ind = row;
        }
    }
    return max_ones_row_ind;
}