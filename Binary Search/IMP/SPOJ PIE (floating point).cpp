//https://www.spoj.com/problems/PIE/
#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;
#define double long double

double pi = 3.14159265358979323846264338327950;

//Find number of friends you can divide these pies with this "size"
//If that number >= friends then true else false
bool canDivide(double size, vector<double> &pie_area, int friends) {
    if (size == 0) return false;
    int friendsCount = 0;
    for (double area : pie_area) {
        friendsCount += (int)(area / size);
    }
    return friendsCount >= friends;
}

double binary_search(vector<double> &pie_area, int friends) {
    int n = pie_area.size();
    sort(pie_area.begin(), pie_area.end());
    //Binary search on the size which are we dividing the pie
    double low = 0;                 //least size one can get is 0
    double high = pie_area[n - 1];  //max size one can get is the max pie area
    while (high - low >= 1e-6) {
        double mid = (low + high) / 2;
        if (canDivide(mid, pie_area, friends)) {
            low = mid;
        } else {
            high = mid;
        }
    }
    return low;
}

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int pies;
        scanf("%d", &pies);
        int friends;
        scanf("%d", &friends);
        friends++;  //for the organizer itself
        vector<int> pie_radius(pies);
        for (int &x : pie_radius) scanf("%d", &x);
        //
        vector<double> pie_area(pies);
        for (int r : pie_radius) {
            pie_area.push_back(r * r * pi);
        }
        printf("%.4Lf\n", binary_search(pie_area, friends));
    }
}
