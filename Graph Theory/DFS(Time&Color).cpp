#include <bits/stdc++.h>
using namespace std;

int V = 10;
/**
* color = 0 if not visited
* color = 1 if entered
* color = 2 if exited
*/
vector<int> color(V, 0);
vector<int> g[V];

vector<int> in_time(V), out_time(V);
int timer = 0;

void dfs(int u) {
    color[u] = 1;  // entered the node
    in_time[u] = timer++;
    for (int v : g[u]) {
        if (color[v] == 0) {
            // if v is not visited yet
            dfs(v);
        } else if (color[v] == 1) {
            // then u->v forms a cycle
            printf("Cycle \n");
        }
    }
    color[u] = 2;  // exited the node
    out_time[u] = timer++;
}

void dfsEntireGraph() {
    for (int u = 0; u < V; ++u) {
        // u is not visited
        if (color[u] == 0) dfs(u);
    }
}