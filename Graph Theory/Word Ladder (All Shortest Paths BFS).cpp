#include <bits/stdc++.h>
using namespace std;

unordered_set<string> dict;
vector<string> generateChildren(string s) {
    vector<string> children;
    for (int i = 0; i < s.length(); ++i) {
        string temp = s;
        for (char c = 'a'; c <= 'z'; ++c) {
            if (s[i] == c) continue;
            temp[i] = c;
            if (dict.find(temp) != dict.end()) children.push_back(temp);
        }
    }
    return children;
}

unordered_map<string, vector<string>> parent;
vector<vector<string>> allPaths;

void generatePaths(string node, vector<string> path, string start) {
    if (node == start) {
        reverse(path.begin(), path.end());
        allPaths.push_back(path);
        return;
    }

    for (string p : parent[node]) {
        path.push_back(p);
        generatePaths(p, path, start);
        path.pop_back();
    }
}

vector<vector<string>> findLadders(string beginWord, string endWord,
                                   vector<string>& wordList) {
    dict.clear();
    parent.clear();
    allPaths.clear();
    for (string word : wordList) dict.insert(word);

    queue<string> q;
    unordered_map<string, int> dist;

    q.push(beginWord);
    dist[beginWord] = 0;
    parent[beginWord] = {};
    bool isFound = false;
    while (!q.empty()) {
        string node = q.front();
        q.pop();
        if (node == endWord) {
            isFound = true;
            break;
        }

        vector<string> children = generateChildren(node);
        for (string child : children) {
            if (dist.find(child) == dist.end()) {  // if seeing child for first time
                dist[child] = dist[node] + 1;
                q.push(child);
                parent[child].push_back(node);
            } else if (dist[child] == dist[node] + 1) {
                // if child already visited but using node->child also we get
                // shortest path so we add node as parent to child
                parent[child].push_back(node);
            }
        }
    }
    if (!isFound) return {};
    // Trace Path
    generatePaths(endWord, {endWord}, beginWord);
    return allPaths;
}