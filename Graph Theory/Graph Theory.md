## **GRAPHS**

Tips = 
- Assume this unless specified = Graph may be disconnected.
- entry/exit time = in_time/out_time

1. BFS (Directed & Undirected)
- For calculating length of shortest path from the source vertex use **dist[]** array.
- For tracing the actual shortest path use **parent[]** array which stores for each vertex the vertex from which we reached it.
- If you need multiple SHORTEST paths then for each index store vector of parents.
- (**IMP**)Finding all shortest paths between source & destination (Word Ladder 2)
- (**IMP**) Find the shortest path of even length from a source vertex s to a target vertex t in an unweighted graph
(For every edge (u, v) add (u, 0) - (v, 1) and (u, 1) - (v, 0) edges. BFS on (s, 0) to (t, 0)). Dist parity is 0 if it is even else is 1 if it is odd.
- Lemma => During the execution of BFS, the queue holding the vertices only contains elements from at max two successive levels of the BFS tree (Used in 0-1 BFS)
- Multi-Source BFS (Kahn's Algorithm, Rotten Oranges)
Problems =
- https://www.geeksforgeeks.org/multi-source-shortest-path-in-unweighted-graph/
- https://www.geeksforgeeks.org/minimum-time-required-so-that-all-oranges-become-rotten/
- (**IMP**)https://www.geeksforgeeks.org/distance-nearest-cell-1-binary-matrix/ (think of this as variation of rotten oranges)
Manhattan Distance b/w 2 cells is nothing but path length in bfs tree.
- Bi-Directional BFS (Finding Path from source to destination)
When To Use = Both initial and goal states are unique and completely defined.
- https://www.interviewbit.com/problems/word-ladder-i/

2. DFS (Directed & Undirected)
- Finds lexicographic first path from source vertex to each vertex.
The edges in adj_list should be in sorted order(If not sorted then sort it.)
- Finds shortest path in a tree (since there will be only simple path) but not in graphs.
- DFS using intime, outtime, color (0 = unvisited, 1 = entered, 2 = exited)
- (**IMP**)ancestor-descendant relationship in a tree = 
1. Using dfs + time , u is ancestor of v if => in_time[u] < in_time[v] && out_time[u] > out_time[v]
2. LCA(u, v) = u then u is ancestor of v | LCA(u, v) = v then v is ancestor of u

Misc =
- https://www.geeksforgeeks.org/count-possible-paths-two-vertices/

3. Cycles
Detection = <br>
- Undirected Graph = same method in both bfs & dfs. if visiting a node which is already visited (vis[v] = true) and it is not parent of u then there is a cycle.
- Directed Graph =
1. DFS = Using back-edge concept by maintaining rec_st[] array
If we are at a node u and we have an edge from u to v but v has been entered but not exited (that means it is currently in recursion stack) which
means u->v is back-edge and cycle is present
2. BFS = Kahn's Algorithm (Using indegree concept)
3. DFS = Coloring Method

Shortest Cycle = <br>
1. In undirected, unweighted graph = for every vertex find all the cycles the node is present in. when you detect compute the length of that cycle.
Since src node is in cycle. dist[u] = src-------u + dist[v] = src----------v + 1 = u------v (cycle length). BFS on every vertex
(https://www.geeksforgeeks.org/shortest-cycle-in-an-undirected-unweighted-graph/)
2. In undirected, weighted graph = for every edge (u, v) remove it from graph then do dijkstra's and find min(dist[u, v] + wt[u, v])

Printing cycles = <br>
1. Printing all cycles in undirected graph (may not work for composite cycles)

4. Topological Sorting (Only for DAG)
- DFS = Gives result in descending order of exit time
- Kahn's Algorithm (BFS) = Done according to indegree
- (**IMP**)Kahn's (using min heap to always get smallest value node in a topo order level) = smallest lexicographic topo order
Time Complexity :- VlogV + E. Each node will be pushed only once & popped only once. (insert, pop happens 2V times) but entire loop runs E times (e1 + e2 + e3 + ...). So overall is VlogV + E.

5. Articulation Points & Bridges (Connected Undirected Graph)
O(V + E) one dfs only
- Using entry time, back edges concept.
- Exploring DFS Tree.

6. Connected Components
- Undirected Graph = Using BFS, DFS
- Directed Graph = Strongly Connected Components (SCC = maximal connected subgraph)
1. Kosaraju's Algo = 2 DFS O(V + E)
```
1. 1st find order of vertices in decreasing order of their finish time (using stack)
2. reverse edges of graph (graph transpose)
3. then do dfs from each node in the finish time decreasing order. all nodes reachable are part of one component. (just like normal undirected graph method but here order of doing dfs is according to the stack)
```
2. Tarjan's Algo = 1 DFS O(V + E)

7. (**IMP**)Shortest Paths (Getting shortest distance & shortest path tracing)
- You do relaxing of adjacent vertices in these algos.
Single-Source =>
- Unweighted Graph = BFS O(E)
- Weighted Graph (0 and 1/x>0) = 0-1 BFS
- Weighted DAG = Topological Sort O(V + E)
- +ve Weighted Graph = Dijkstra's O((V + E) * logV)
- Both +ve & -ve weighted graph (No -ve weight cycle) = Bellman Ford O(VE)
Shortest path distance from one node to all other nodes, shortest path tracing, -ve weight cycle detection & printing that cycle

All-pair shortest paths =>
- +ve/-ve weighted graph = Floyd-Washall Algorithm O(V^3)

Misc Problems =
- (**IMP**)https://www.geeksforgeeks.org/minimum-cost-path-from-source-node-to-destination-node-via-an-intermediate-node/
For every node u do this => dist(src, u) + dist(inter, u) + dist(dest, u)

8. Negative weight cycle detection in a graph => (TODO)
- https://www.geeksforgeeks.org/detect-negative-cycle-graph-bellman-ford/
Try for disconnected graph also
- https://www.geeksforgeeks.org/detecting-negative-cycle-using-floyd-warshall/?ref=rp

9. Spanning Trees (Connected & Undirected Graph)
- Graph(V, E) = Spanning Tree(V, V - 1)
- (**IMP**)Minimum spanning tree is also the tree with minimum product of weights of edges. (It can be easily proved by replacing the weights of all edges with their logarithms)
- The maximum spanning tree (spanning tree with the sum of weights of edges being maximum) of a graph can be obtained similarly to that of the minimum spanning tree, by changing the signs of the weights of all the edges to their opposite and then applying any of the minimum spanning tree algorithm.

- MST = Prims Algorithm (Dense)
1. Dense Graphs + Adj. Matrix = O(V^2)
2. Sparse Graphs + Adj. List + Set = O(VlogV + ElogV)
3. With heap (updating key function supported in logV) = O(VlogV + ElogV)

- MST = Krushkal Algorithm (Sparse)
1. Simple Implementation = O(VE + ElogE)
2. Using DSU = O(ElogE + ElogV)

Prims vs Kruskal
If in question you are getting vector of edges [(u, v, wt)] then use krushkal. for prims you need to change the structure of graph
```
Use Prim's algorithm when you have a graph with lots of edges.
For a graph with V vertices E edges, Kruskal's algorithm runs in O(E log V) time and Prim's algorithm can run in O(E + V log V) amortized time, if you use a Fibonacci Heap.
Prim's algorithm is significantly faster in the limit when you've got a really dense graph with many more edges than vertices. Kruskal performs better in typical situations (sparse graphs) because it uses simpler data structures.

```

10. Disjoint-Set Union
- union by rank & find using path by compression = O(logN)
- nearly constant time on average (O(1) amortized time)
- If in question you are getting vector of edges [(u, v, wt)] then use DSU
Applications =
1. krushkal 
2. cycle detection

11. Bipartite Graph
- It should not have odd-length cycle
- Bipartite graph should be 2-colorable
- (**IMP**)https://www.geeksforgeeks.org/check-if-there-is-a-cycle-with-odd-weight-sum-in-an-undirected-graph/?ref=rp



* https://www.geeksforgeeks.org/find-a-mother-vertex-in-a-graph/
Array problems as graph
* https://www.geeksforgeeks.org/minimum-number-swaps-required-sort-array/
https://practice.geeksforgeeks.org/problems/snake-and-ladder-problem/0
You will be getting a stream of edges for a directed graph. You have to tell at what point the cycle is formed. (My approach was using Disjoint sets ). He asked if I can implement the same algorithm to the undirected graph also? He asked me to implement Disjoint sets algorithm and to make it efficient using union by rank and path compression.


* Deciphering given problem to graph =
- https://leetcode.com/problems/regions-cut-by-slashes/
- https://www.interviewbit.com/problems/cows-and-snacks/?ref=random-problem
