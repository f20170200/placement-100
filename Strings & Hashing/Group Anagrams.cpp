#include <bits/stdc++.h>
using namespace std;

//Only lowercase letters (so think of using vector<int>(26))

//O(N^2 + N*L) Time
vector<vector<int>> anagrams1(const vector<string> &words) {
    int N = words.size();
    vector<vector<int>> result;

    unordered_map<int, unordered_map<char, int>> freqMap;  //wordIndex -> freqMap
    //O(N * L)
    for (int i = 0; i < N; ++i) {
        for (char c : words[i])
            freqMap[i][c]++;
    }
    vector<int> visited(N, 0);
    //O(N * N) in the worst case when no anagrams present
    for (int i = 0; i < N; ++i) {
        if (visited[i]) continue;
        visited[i] = true;
        vector<int> temp = {i + 1};
        for (int j = i; j < N; ++j) {
            if (visited[j]) continue;
            if (freqMap[i] == freqMap[j]) {
                visited[j] = true;
                temp.push_back(j + 1);
            }
        }
        result.push_back(temp);
    }
    return result;
}

//O(N + N * L) Time = More Efficient
vector<vector<int>> anagrams2(const vector<string> &words) {
    int N = words.size();
    vector<vector<int>> result;

    vector<vector<int>> freqMap(N, vector<int>(26, 0));  //wordIndex -> freqMap
    unordered_map<string, vector<int>> keyMap;           //freq key -> all word indexes which have that key
    //O(N * (L + 26)) where L is maximum length of all words
    for (int i = 0; i < N; ++i) {
        for (char c : words[i])
            freqMap[i][c - 'a']++;
        string key = "";
        for (int c = 0; c < 26; ++c)
            key += to_string(freqMap[i][c]);
        keyMap[key].push_back(i + 1);
    }
    //O(N)
    for (auto it : keyMap) {
        result.push_back(it.second);
    }
    return result;
}
