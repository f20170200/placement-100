#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.interviewbit.com/problems/equal/
 * Multiple Solutions then print lexicographically smallest element.
 * Extend for printing all solutions (shouldn't contain duplicate quadruplets). And printing in lexicographic order.
 * Will this work for duplicate elements?
 * TODO: Variation = https://www.geeksforgeeks.org/find-four-elements-a-b-c-and-d-in-an-array-such-that-ab-cd/
 */

vector<int> compare(vector<int> a, vector<int> b) {
    if (a.empty())
        return b;
    for (int i = 0; i < 4; ++i) {
        if (a[i] < b[i])
            return a;
        if (a[i] > b[i])
            return b;
    }
    return a;  //both are same
}

vector<int> equal(vector<int> &arr) {
    unordered_map<int, pair<int, int>> sumMap;  //sum->{i,j}
    int N = arr.size();
    vector<int> result = {};
    for (int i = 0; i < N; ++i) {
        for (int j = i + 1; j < N; ++j) {
            int sum = arr[i] + arr[j];
            if (sumMap.find(sum) != sumMap.end()) {
                pair<int, int> p = sumMap[sum];
                if (p.first != i && p.second != i && p.first != j && p.second != j) {
                    auto vec = {p.first, p.second, i, j};
                    result = compare(result, vec);
                }
            } else {
                sumMap[sum] = {i, j};
            }
        }
    }
    return result;
}
