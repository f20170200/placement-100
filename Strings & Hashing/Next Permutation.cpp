#include <bits/stdc++.h>
using namespace std;

vector<int> nextPermutation(vector<int> &A) {
    int N = A.size();
    int startIndex = N - 1;  // starting index of decreasing subarray
    while (startIndex >= 1 && A[startIndex - 1] > A[startIndex]) {
        startIndex--;
    }
    if (startIndex != 0) {
        // find next greater element to A[startIndex-1] in [startIndex...N-1]
        // subarray
        int greaterIndex;
        for (int i = N - 1; i >= startIndex; --i) {
            if (A[startIndex - 1] < A[i]) {
                greaterIndex = i;
                break;
            }
        }
        // swap A[sI] & A[gI]
        int temp = A[greaterIndex];
        A[greaterIndex] = A[startIndex - 1];
        A[startIndex - 1] = temp;
    }
    // reverse [startIndex...N-1] subarray
    reverse(A.begin() + startIndex, A.end());
    return A;
}
