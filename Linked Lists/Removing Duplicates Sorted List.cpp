#include "LinkedList.h"

/**
 * Delete duplicates of each node such that in the final result there are no duplicates
 * https://www.interviewbit.com/problems/remove-duplicates-from-sorted-list/
 */
Node* deleteDuplicates1(Node* head) {
    if (!head || head->next == NULL)
        return head;

    Node* trav = head;
    while (trav != NULL) {
        Node* curr = trav;
        while (trav->next != NULL && trav->val == trav->next->val)
            trav = trav->next;
        trav = trav->next;
        curr->next = trav;
    }
    return head;
}

/**
 * Delete all the duplicates of the node. In the final result only the nodes which were distinct should appear
 * https://www.interviewbit.com/problems/remove-duplicates-from-sorted-list-ii/
 */
Node* deleteDuplicates2(Node* head) {
    if (!head || head->next == NULL)
        return head;

    Node *prev = NULL, *curr = head;
    while (curr != NULL && curr->next != NULL) {
        if (curr->val != curr->next->val) {
            prev = curr;
            curr = curr->next;
            continue;
        }
        while (curr->next != NULL && curr->val == curr->next->val)
            curr = curr->next;
        if (prev == NULL)
            head = curr->next;
        else
            prev->next = curr->next;
        curr = curr->next;
    }
    return head;
}
