#include <bits/stdc++.h>
using namespace std;

//Generate all subsets where array has duplicate numbers

vector<vector<int>> result;

//Choice space = [start...N-1]. Don't use [0...start-1]
//Guarentees lexicograph order if arr is sorted
void backtrack(vector<int> &arr, int start = 0, vector<int> subset = {}) {
    result.push_back(subset);

    for (int i = start; i < arr.size(); ++i) {
        //we already included the same number previously. we will get duplicates if we include again
        if (i != start && arr[i] == arr[i - 1]) continue;
        subset.push_back(arr[i]);  //our choice = include it
        backtrack(arr, i + 1, subset);
        subset.pop_back();  //undo choice = exclude it and go to next number
    }
}

vector<vector<int>> subsetsWithDup(vector<int> &arr) {
    result.clear();
    sort(arr.begin(), arr.end());
    backtrack(arr);
    return result;
}
