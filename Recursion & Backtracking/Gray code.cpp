#include <bits/stdc++.h>
using namespace std;

//https://www.interviewbit.com/problems/gray-code/ (IMP Problem.)

//codeMask will be global. any change in bit should reflect in prev. recursive calls also.
//draw recursive tree to understand
int codeMask = 0;

void flip(int i) {
    //if ith bit is set then clear it
    if (codeMask & (1 << i)) {
        codeMask = codeMask & (~(1 << i));
    } else {
        codeMask = codeMask | (1 << i);
    }
}

//index = 0,1,2...N-1
void grayCodeRecursive(int index, int N, vector<int> &result) {
    if (index == N) {
        result.push_back(codeMask);
        return;
    }

    //dont do anything
    grayCodeRecursive(index + 1, N, result);
    //flip ith bit in codeMask. this flipping should reflect in prev. recursive calls also since we need only one bit different
    flip(index);
    grayCodeRecursive(index + 1, N, result);
}

vector<int> grayCode(int N) {
    int codeMask = 0;  //initially contains N 0-bits

    vector<int> result;
    grayCodeRecursive(0, N, result);
    return result;
}
