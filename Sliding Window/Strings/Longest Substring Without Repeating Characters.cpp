#include <bits/stdc++.h>
using namespace std;

//https://leetcode.com/problems/longest-substring-without-repeating-characters/

//Time O(N) but in the worst case it takes 2N steps. Optimized version can be done using N steps.
int lengthOfLongestSubstring(string word) {
    int N = word.length();
    unordered_set<char> seen;
    int maxLen = 1;

    int left = 0;
    for (int right = 0; right < N;) {
        char c = word[right];
        if (seen.find(c) == seen.end()) {
            seen.insert(c);
            maxLen = max(maxLen, right - left + 1);
            right++;
        } else {
            seen.erase(word[left++]);
        }
    }
    return maxLen;
}

//Time O(N). Only N steps. Optimized Version
int lengthOfLongestSubstring(string S) {
    int maxLen = 1;
    unordered_map<char, int> indexMap;  //c -> last seen index
    int left = 0, right = 0;

    while (right < S.length()) {
        char c = S[right];
        //2nd condition is when that c was already removed from the window
        if (indexMap.find(c) == indexMap.end() || indexMap[c] < left) {
            indexMap[c] = right;
            maxLen = max(maxLen, right - left + 1);
            right++;
        } else {
            left++;
        }
    }
    return maxLen;
}