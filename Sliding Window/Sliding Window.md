## String based Sliding Window
1. Use two pointers: start and end to represent a window.
2. Move end to find a valid window.
3. When a valid window is found, increment start according to the problem

Problems =
https://leetcode.com/problems/minimum-window-substring/
https://leetcode.com/problems/longest-substring-without-repeating-characters/
https://leetcode.com/problems/substring-with-concatenation-of-all-words/
https://leetcode.com/problems/find-all-anagrams-in-a-string/
https://www.lintcode.com/en/old/problem/longest-substring-with-at-most-k-distinct-characters/
https://leetcode.com/problems/number-of-substrings-containing-all-three-characters/

Different than the above template
1. https://www.geeksforgeeks.org/length-smallest-sub-string-consisting-maximum-distinct-characters/
2. https://www.geeksforgeeks.org/smallest-window-contains-characters-string/ 



TODO:
count substrings where each char occurs atmost k times




