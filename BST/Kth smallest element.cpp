#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/find-k-th-smallest-element-in-bst-order-statistics-in-bst/
 * https://www.geeksforgeeks.org/kth-smallest-element-in-bst-using-o1-extra-space/
 * These approaches are same if you want find kth element in binary tree
 * 
 * 1. Using inorder array and find kth element which is kth smallest element since sorted
 *    O(N) Time | O(h) Space
 * 2. Can do inorder traversal using morris traversal
 *    O(N) Time | O(1) Space
 * 3. Augmented BST. Modify node structure. Store count of nodes in the left-subtree
 *    O(height) Time | O(height) Space
 */