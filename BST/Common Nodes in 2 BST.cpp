#include "Tree.h"

/*
0. For each node in the shorter BST, search in the other BST
O(min(M, N) * log(max(M, N))) Time | O(h) recursion stack space

1. Finding inorder arrays of both BSTs and finding intersection of two sorted arrays
inorder => recursion / iteration + stack / iteration + constant space (morris)
O(M + N) Time | O(M + N) Space to store arrays
Intersection => O(min(M, N)) Time | O(1) Time

2. Only one traversal. Using forward BST iterators in the BSTs and following the intersection logic
O(M + N) Time | O(h1 + h2) Space for iterators
*/

//1st method
void inorder(Node* root, vector<int>& arr) {
    if (root != NULL) {
        inorder(root->left, arr);
        arr.push_back(root->data);
        inorder(root->right, arr);
    }
}

vector<int> intersection(vector<int>& a1, vector<int>& a2) {
    vector<int> result;
    int i = 0, j = 0;
    while (i < a1.size() && j < a2.size()) {
        if (a1[i] == a2[j]) {
            result.push_back(a1[i]);
            i++;
            j++;
        } else if (a1[i] < a2[j]) {
            //Then a1[i] won't be present in a2 so skip it
            i++;
        } else {
            j++;
        }
    }
    return result;
}

//Return all intersection nodes in sorted manner
vector<int> solve(Node* root1, Node* root2) {
    vector<int> inorder1, inorder2;
    inorder(root1, inorder1);
    inorder(root2, inorder2);
    int sum = 0;
    return intersection(inorder1, inorder2);
}