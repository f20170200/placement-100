#include "../Tree.h"

class ForwardIterator {
   private:
    stack<Node*> st;  //O(h) space in worst space

   public:
    ForwardIterator(Node* root) {
        //top of stack should point to left most node in the BST (inorder[0] element)
        while (root != NULL) {
            st.push(root);
            root = root->left;
        }
    }
    //Should be similar to incrementing a pointer
    void next() {
        if (st.empty()) {
            return;
        }
        //In the right subtree of current node go to the leftmost node
        Node* curr = st.top()->right;
        st.pop();
        if (!curr) {
            return;
        }
        while (curr != NULL) {
            st.push(curr);
            curr = curr->left;
        }
    }

    Node* current() {
        return (st.empty() ? NULL : st.top());
    }

    //Return true if there are still elements left
    bool hasNext() {
        return !st.empty();
    }
};

int main() {
    Node* root = new Node(5);
    root->left = new Node(1);
    root->left->left = new Node(0);
    root->left->right = new Node(4);
    root->right = new Node(10);
    root->right->left = new Node(7);
    root->right->left->right = new Node(9);
    ForwardIterator it(root);
    while (it.hasNext()) {
        cout << it.current()->data << " ";
        it.next();
    }
}