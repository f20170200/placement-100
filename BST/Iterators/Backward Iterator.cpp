#include "../Tree.h"

class BackwardIterator {
   private:
    stack<Node*> st;  //O(h) space in worst space

   public:
    BackwardIterator(Node* root) {
        //top of stack should point to right most node in the BST (inorder[n-1] element)
        while (root != NULL) {
            st.push(root);
            root = root->right;
        }
    }
    //Should be similar to decrementing a pointer
    void next() {
        if (st.empty()) {
            return;
        }
        //In the left subtree of current node go to the rightmost node
        Node* curr = st.top()->left;
        st.pop();
        if (!curr) {
            return;
        }
        while (curr != NULL) {
            st.push(curr);
            curr = curr->right;
        }
    }

    Node* current() {
        return (st.empty() ? NULL : st.top());
    }

    //Return true if there are still elements left
    bool hasNext() {
        return !st.empty();
    }
};