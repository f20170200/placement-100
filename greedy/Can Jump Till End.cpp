#include <bits/stdc++.h>
using namespace std;

/**
 * https://leetcode.com/problems/jump-game/
 * There is a simple dp solution both top-down and bottom-up
 * Takes O(N^2) Time | O(N) Space
 * However if we carefully observe
 * At an index i we only one index to its right which should be GOOD (since after we found such index we break)
 * That means it's better to keep track of leftmost good index towards the right of this index 
 * and check if currIndex+nums[currIndex] >= leftmost_good_index then that means we can reach the end.
 */

int dp[100000];
bool top_down(vector<int>& nums, int ind) {
    if (ind == nums.size() - 1)
        return true;

    if (dp[ind] != -1)
        return dp[ind];

    int maxJump = nums[ind];
    for (int k = ind + 1; k <= ind + maxJump; ++k) {
        if (top_down(nums, k))
            return dp[ind] = true;
    }
    return dp[ind] = false;
}
bool canJump(vector<int>& nums) {
    memset(dp, -1, sizeof(dp));
    return top_down(nums, 0);

    //Bottom-up
    int n = nums.size();
    dp[n - 1] = true;
    for (int i = n - 2; i >= 0; --i) {
        int maxReach = min(n - 1, i + nums[i]);
        for (int k = i + 1; k <= maxReach; ++k) {
            if (dp[k] == true) {
                dp[i] = true;
                break;
            }
        }
        if (dp[i] == -1)
            dp[i] = false;
    }
    return dp[0];
}

//O(N) Time | O(1) Space
bool canJump(vector<int>& nums) {
    if (nums.empty())
        return false;
    int n = nums.size();
    //Careful observation => at an index i we only one index to its right which should be GOOD
    //That means it's better to keep track of leftmost good index and check if currIndex+nums[currIndex] >= leftmost_good_index then that means we can reach the end
    int leftmost_good_index = n - 1;
    for (int i = n - 2; i >= 0; --i) {
        if (i + nums[i] >= leftmost_good_index) {
            //i will be the leftmost_good_index now
            leftmost_good_index = i;
        }
    }
    return (leftmost_good_index == 0);
}