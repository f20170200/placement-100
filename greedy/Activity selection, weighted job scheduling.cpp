#include <bits/stdc++.h>
using namespace std;

//Activity selection problem = maximize non-overlapping intervals
//Variation = https://www.interviewbit.com/problems/chain-of-pairs/
struct Activitiy {
    int start, finish;
};
bool activityCompare(Activitiy s1, Activitiy s2) {
    return (s1.finish < s2.finish);
}
void printMaxActivities(Activitiy arr[], int n) {
    // Sort jobs according to finish time
    sort(arr, arr + n, activityCompare);

    // The first activity always gets selected
    int i = 0;
    //print interval arr[i]

    // Consider rest of the activities
    for (int j = 1; j < n; j++) {
        // If this activity has start time greater than or
        // equal to the finish time of previously selected
        // activity, then select it
        if (arr[i].finish <= arr[j].start) {
            //print interval arr[j]
            i = j;
        }
    }
}

/**
 * Variation of both activity selection problem and job scheduling problem
 * https://www.geeksforgeeks.org/weighted-job-scheduling/
 * https://www.spoj.com/problems/RENT/
 * For each activity we have {start, end, profit}
 * Get the max. profit for non-overlapping intervals
 * Activity selection = weighted job schedling when weight = 1
 * Job scheduling = weighted job scheding when start = deadline-1 and end=deadline
 * Sort by end time and then do dp
 * max_profit(n) = max(
 *                  max_profit(n - 1) //ignore this interval
 *                  profit[n] + max_profit(i)) // where i is the first non-overlapping interval with this interval
 */
int dp[10001];

struct Job {
    int start, end, profit;
    Job(int s, int e, int p) : start(s), end(e), profit(p) {}
};

bool comp(Job a, Job b) {
    return a.end < b.end;
}

int latestNonOverlap(vector<Job> &jobs, int index) {
    //In [0...index-1] find the right most non-overlapping interval with this index
    //Can use linear search.
    //Use binary search since predicate will be TTTTTFFFFF find last T (true is non-overlapping)
    int low = 0, high = index - 1;
    while (low < high) {
        int mid = low + (high - low + 1) / 2;
        if (jobs[mid].end < jobs[index].start) {
            low = mid;
        } else {
            high = mid - 1;
        }
    }
    //Do sanity check
    if (jobs[low].end < jobs[index].start)
        return low;
    else
        return -1;
}

int maxProfitRecursive(vector<Job> &jobs, int index) {
    if (index == 0) return jobs[0].profit;

    if (dp[index] != -1)
        return dp[index];

    //2 choices for this interval = Include it or exclude it
    int nonoverlapIndex = latestNonOverlap(jobs, index);

    if (nonoverlapIndex == -1)
        return dp[index] = max(maxProfitRecursive(jobs, index - 1), jobs[index].profit);
    else
        return dp[index] = max(
                   maxProfitRecursive(jobs, index - 1),                            //exclude it
                   jobs[index].profit + maxProfitRecursive(jobs, nonoverlapIndex)  //include it
               );
}

int weightedJobScheduling(vector<Job> &jobs, int n) {
    memset(dp, -1, sizeof(dp));
    //Sort by end time
    sort(jobs.begin(), jobs.end(), comp);

    return maxProfitRecursive(jobs, n - 1);
}