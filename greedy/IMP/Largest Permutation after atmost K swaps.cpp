#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.interviewbit.com/problems/largest-permutation/
 * Given an integer array A of size N consisting of unique integers from 1 to N. You can swap any two integers atmost B times.
 * Return the largest lexicographical value array that can be created by executing atmost B swaps.
 */

//Try to put larger numbers in the MSB positions
vector<int> solve(vector<int> &A, int B) {
    unordered_map<int, int> indexMap;  //number -> index
    int N = A.size();
    for (int i = 0; i < N; ++i) {
        indexMap[A[i]] = i;
    }

    int ind = 0;
    int n = N;
    if (B >= N) {
        sort(A.begin(), A.end(), greater<int>());
        return A;
    }
    while (B > 0 && ind < N) {
        if (A[ind] < n) {
            int num = A[ind];
            swap(A[ind], A[indexMap[n]]);
            int temp = indexMap[n];
            indexMap[n] = ind;
            indexMap[num] = temp;
            B--;
        }
        ind++;
        n--;
    }
    return A;
}
