#include <bits/stdc++.h>
using namespace std;

//https://www.interviewbit.com/problems/distinct-subsequences/

//count number of times B is present as subsequence in A
int numDistinct(string A, string B) {
    int n = A.length(), m = B.length();
    int dp[n + 1][m + 1];

    //Base cases
    for (int j = 0; j <= m; ++j) {
        dp[0][j] = 0;
    }
    for (int i = 0; i <= n; ++i) {
        dp[i][0] = 1;
    }

    //Recurrence
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= m; ++j) {
            if (A[i - 1] == B[j - 1]) {
                dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j];
            } else {
                dp[i][j] = dp[i - 1][j];
            }
        }
    }
    return dp[n][m];
}

int numDistinctOptimized(string A, string B) {
    int n = A.length(), m = B.length();
    vector<int> dp_i(m + 1, 0);  //initially for 0th row (0 size A)
    dp_i[0] = 1;

    for (int i = 1; i <= n; ++i) {
        for (int j = m; j >= 1; --j) {
            if (A[i - 1] == B[j - 1]) {
                dp_i[j] = dp_i[j - 1] + dp_i[j];
            }
        }
    }
    return dp_i[m];
}