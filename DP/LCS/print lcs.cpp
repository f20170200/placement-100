#include <bits/stdc++.h>
using namespace std;

int dp[10000][10000];

int LCS(int n1, int n2, string A, string B) {
    if (n1 == 0 || n2 == 0) {
        return 0;
    }

    if (dp[n1][n2] != -1) {
        return dp[n1][n2];
    }

    if (A[n1 - 1] == B[n2 - 1]) {
        return dp[n1][n2] = 1 + LCS(n1 - 1, n2 - 1, A, B);
    }

    return dp[n1][n2] = max(LCS(n1, n2 - 1, A, B), LCS(n1 - 1, n2, A, B));
}

void printLCS(string A, string B) {
    memset(dp, -1, sizeof(dp));
    cout << "Length is " << LCS(A.length(), B.length(), A, B) << endl;

    string lcs_string = "";
    int n1 = A.length(), n2 = B.length();
    //n1 and n2 are length so that's why > 0
    while (n1 > 0 && n2 > 0) {
        if (A[n1 - 1] == B[n2 - 1]) {
            lcs_string = A[n1 - 1] + lcs_string;
            n1--;
            n2--;
        } else {
            if (dp[n1][n2] == dp[n1 - 1][n2]) {
                n1--;
            } else {
                n2--;
            }
        }
    }
    cout << "lcs_string is " << lcs_string << endl;
}

int main() {
    string A = "abcdgh";
    string B = "abedfh";
    printLCS(A, B);
    printLCS("ab", "ab");
}