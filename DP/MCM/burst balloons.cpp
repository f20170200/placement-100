#include <bits/stdc++.h>
using namespace std;

//https://leetcode.com/problems/burst-balloons/

int maxCoinsRecursiveHelper(int i, int j, vector<int>& A) {
    //Base
    if (i > j) return 0;
    int n = (int)A.size();
    if (i == j) return (i ? A[i - 1] : 1) * A[i] * ((j == n - 1) ? 1 : A[i + 1]);
    if (dp[i][j] != -1) return dp[i][j];
    //Choice

    int res = 0;
    for (int k = i; k <= j; ++k) {
        //kth balloon is the last one to burst in the sequence in A[i...j]
        res = max(res, maxCoinsRecursiveHelper(i, k - 1, A) + maxCoinsRecursiveHelper(k + 1, j, A) +
                           A[k] * (i == 0 ? 1 : A[i - 1]) * (j == (n - 1) ? 1 : A[j + 1]));
    }
    return (dp[i][j] = res);
}
int maxCoinsRecursive(vector<int> A) {
    if (A.empty()) return 0;  //corner case
    int n = A.size();
    memset(dp, -1, sizeof(dp));
    return maxCoinsRecursiveHelper(0, n - 1, A);
}

int maxCoinsIterative(vector<int>& nums) {
    int n = nums.size();
    if (n == 0) {
        return 0;
    }
    int dp[n][n];
    //dp[i][j] = maximum coins we get by bursting all balloons inside [i..j]

    for (int i = 0; i < n; ++i) {
        dp[i][i] = (i == 0 ? 1 : nums[i - 1]) *
                   nums[i] *
                   (i == n - 1 ? 1 : nums[i + 1]);
    }

    //Fill diagonally
    for (int len = 1; len < n; ++len) {
        for (int i = 0; i < n; ++i) {
            int j = len + i;
            if (j >= n) {
                break;
            }
            dp[i][j] = 0;
            /*
                    we will burst kth balloon after bursting (i...k-1) and (k+1...j)
                    so now k will have (i - 1)th and (j + 1)th as adjacent
                */
            for (int k = i; k <= j; ++k) {
                int result = (k == 0 || i > k - 1 ? 0 : dp[i][k - 1]) +
                             (k == n - 1 || k + 1 > j ? 0 : dp[k + 1][j]) +
                             (i == 0 ? 1 : nums[i - 1]) * nums[k] *
                                 (j == n - 1 ? 1 : nums[j + 1]);

                dp[i][j] = max(dp[i][j], result);
            }
        }
    }

    return dp[0][n - 1];
}