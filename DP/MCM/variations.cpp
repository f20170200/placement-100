#include <bits/stdc++.h>
using namespace std;

/*
Boolean parenthesization =>
Number of ways to evaluate the expression to true
operators = | , & , ^

    dp[i][j][0] = number of ways to eval expr[i...j] to T
    dp[i][j][1] = number of ways to eval expr[i...j] to F

    Base Case
    dp[i][i][0] = expr[i] == T
    dp[i][i][1] = expr[i] == F

    Recurrence
    //k will point to each operator
    for(int k = i + 1; k < j; k += 2) {
        e1 = (i, k - 1)
        e2 = (k + 1, j)
        op = expr[k]

        if(op == ^)
            (e, T) ways = (e1, T) ways * (e2, F) ways + (e1, F) ways * (e2, T) ways

        if(op == &)
            (e, T) ways = (e1, T) ways * (e2, T) ways
    }
*/