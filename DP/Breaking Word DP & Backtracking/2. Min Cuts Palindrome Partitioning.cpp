#include <bits/stdc++.h>
using namespace std;

/**
* Given a string cut into segments such that each segment is palindrome.
* Find min cuts needed.
*/

#define P isP
bool isP[1001][1001];

int dp[1001];  // dp[i] = [i....N-1] min cuts needed
int recursive(string s, int ind) {
    if (ind >= s.length()) {
        return 0;
    }

    if (dp[ind] != -1) {
        return dp[ind];
    }

    if (isP[ind][s.length() - 1]) {
        // if [ind...N-1] is itself palindrome then 0 cuts
        return dp[ind] = 0;
    }
    dp[ind] = INF;
    for (int i = ind; i < s.length(); ++i) {
        if (isP[ind][i]) {
            dp[ind] = min(dp[ind], 1 + recursive(s, i + 1));
        }
    }
    return dp[ind];
}

int solution() {
    memset(dp, -1, sizeof(dp));
    string str;
    cin >> str;
    int n = str.length();

    // Find all palindromic substrings of the string.
    int i, j, k, L;
    for (i = 0; i < n; i++) {
        P[i][i] = true;
    }
    for (L = 2; L <= n; L++) {
        for (i = 0; i < n - L + 1; i++) {
            j = i + L - 1;
            if (L == 2)
                P[i][j] = (str[i] == str[j]);
            else
                P[i][j] = (str[i] == str[j]) && P[i + 1][j - 1];
        }
    }
    //
    int ans = recursive(str, 0);
    if (ans >= INF) {
        return -1;
    }
    return ans;
}