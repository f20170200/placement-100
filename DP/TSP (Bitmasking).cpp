int minCost;

/*
Starting node is 0. Cover a hamiltonioan cycle from 0 (visting every vertex once
and returning back to 0)
*/

// 6543210
// 0000000
// O(N-1)! since starting node is fixed.
// if not fixed then trying all possible starting nodes = O(N!)
void backtrack(vector<vector<int>> &costs, int N, int u, int costTillNow,
               int visitedMask) {
    if (visitedMask == (1 << N) - 1) {
        // all cities visited
        minCost = min(minCost, costTillNow + costs[u][0]);
        return;
    }

    for (int v = 0; v < N; ++v) {
        if (u == v) continue;
        if (visitedMask & (1 << v)) {
            // if vth bit is set then it it is visited
            continue;
        }
        backtrack(costs, N, v, costTillNow + costs[u][v],
                  visitedMask | (1 << v));
    }
}

// O(N ^ 2 * 2 ^ N) Time | O(N * 2 ^N) Space
int top_down(vector<vector<int>> &costs, int N, int u, int mask,
             vector<vector<int>> &dp) {
    if (mask == (1 << N) - 1) {
        return dp[u][mask] = costs[u][0];
    }

    if (dp[u][mask] != -1) return dp[u][mask];

    int ans = INT_MAX;
    for (int v = 0; v < N; ++v) {
        // if(u == v) continue; redundant since u will be visited already
        if (mask & (1 << v)) continue;
        ans =
            min(ans, costs[u][v] + top_down(costs, N, v, mask | (1 << v), dp));
    }
    return dp[u][mask] = ans;
}

int solve(int N, vector<vector<int>> &costs) {
    // minCost = INT_MAX;
    // backtrack(costs, N, 0, 0, 1);
    // return minCost;
    vector<vector<int>> dp(N, vector<int>(1 << N, -1));
    return top_down(costs, N, 0, 1, dp);
}
