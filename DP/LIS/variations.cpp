#include <bits/stdc++.h>
using namespace std;

/*
Minimum deletions to make an array sorted
    Answer = array size - LIS of the array

Maximum sum increasing subsequence
of all increasing subsequences find the one which has max. sum
    Answer = same as LIS but here dp[i] = max. sum of increasing subsequenc ending at i
    int dp[n]
    for(int i = 0; i < n; ++i)
        dp[i] = arr[i]; //initially it is arr[i]

    for(int i = 1; i < n; ++i) {
        for(int j = 0; j < i; ++j) {
            if(arr[j] < arr[i]) { //checking for increasing subsequence
                if(dp[j] + arr[i] > dp[i]) {
                    dp[i] = dp[j] + arr[i]
                }
            }
        }
    }

    answer = max(dp[i])
*/