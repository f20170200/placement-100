#include <bits/stdc++.h>
using namespace std;

struct Node {
    int data;
    struct Node *left, *right, *random;
    Node(int x) : data(x), left(NULL), right(NULL), random(NULL) {}
};

/*
1. Using Hashing
*/
unordered_map<Node *, Node *> mappings;  //original node -> clone node
Node *cloneRecursive(Node *root) {
    if (!root) {
        return NULL;
    }

    Node *cloneRoot = new Node(root->data);
    cloneRoot->left = cloneRecursive(root->left);
    cloneRoot->right = cloneRecursive(root->right);
    mappings[root] = cloneRoot;
    return cloneRoot;
}

void attachRandom(Node *root, Node *&cloneRoot) {
    if (!root) {
        return;
    }

    if (root->random != NULL) {
        Node *originalRandom = root->random;
        Node *cloneRandom = mappings[originalRandom];
        cloneRoot->random = cloneRandom;
    }
    attachRandom(root->left, cloneRoot->left);
    attachRandom(root->right, cloneRoot->right);
}

Node *cloneTree(Node *root) {
    Node *cloneRoot = cloneRecursive(root);
    attachRandom(root, cloneRoot);
    return cloneRoot;
}

/*
2. By Modifying Original Tree = Similar To Linked List Approach. 
    1. adding cloned node to left of the original node (we can either attach on left or right)
    2. then attaching random pointers
    3. then separating the cloned nodes from tree
*/
void insertClone(Node *root) {
    if (!root) {
        return;
    }

    Node *left = root->left;
    root->left = new Node(root->data);
    root->left->left = left;
    insertClone(root->left->left);
    insertClone(root->right);
}

void linkRandomPointer(Node *root) {
    if (!root) {
        return;
    }

    if (root->random != NULL) {
        root->left->random = root->random->left;
    }
    linkRandomPointer(root->left->left);
    linkRandomPointer(root->right);
}

Node *separateClonedTree(Node *root) {
    if (!root) {
        return NULL;
    }

    Node *originalLeft = root->left->left;
    Node *cloneNode = root->left;
    cloneNode->left = separateClonedTree(originalLeft);
    cloneNode->right = separateClonedTree(root->right);
    root->left = originalLeft;
    return cloneNode;
}

Node *cloneTree(Node *root) {
    if (!root) {
        return NULL;
    }

    insertClone(root);
    linkRandomPointer(root);
    return separateClonedTree(root);
}